<?php


namespace App\Controller;

use RuntimeException;
use App\Entity\Vegetal;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[AsController]

final class VegetalImageController extends AbstractController {

    public function __invoke(Request$request)
    {
        $post = $request->attributes->get('data');
        if (!($post instanceof Vegetal)) {
            throw new RuntimeException('Vegetal attendu');
        }
        
        $post->setImageFile($request->files->get('file'));
        $post->setUpdatedAt(new \DateTime());
        dd($post); 
        return $post;
    }

}
   