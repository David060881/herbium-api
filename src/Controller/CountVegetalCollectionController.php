<?php

namespace App\Controller;

use App\Repository\VegetalRepository;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

#[AsController]
class CountVegetalCollectionController extends AbstractController
{
    public function __construct(private VegetalRepository $vegetalRepository)
    {}

    public function __invoke(Request $request):int
    {
        $onlineQuery =  $request->get('online');
        $conditions = [];
        if($onlineQuery != null){ 
            $conditions = ['online' => $onlineQuery == '1' ? true :false];
        }
        return $this->vegetalRepository->count($conditions);
    }
}
