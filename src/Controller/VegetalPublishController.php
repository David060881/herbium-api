<?php


namespace App\Controller;


use App\Entity\Vegetal;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;



#[AsController]
class VegetalPublishController extends AbstractController
{

    public function __invoke(Vegetal $data): Vegetal
    {
        $data->setOnline(true);
        return $data;
    }
}
