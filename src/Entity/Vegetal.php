<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\VegetalRepository;
use ApiPlatform\Metadata\GetCollection;
use App\Controller\VegetalImageController;
use App\Controller\VegetalPublishController;
use Symfony\Component\HttpFoundation\File\File;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use App\Controller\CountVegetalCollectionController;
use ApiPlatform\OpenApi\Model;


//#[Post(security: "is_granted('ROLE_ADMIN')")]
//#[Put(security: "is_granted('ROLE_ADMIN')")]


/**
 * @Vich\Uploadable
 */
#[ORM\Entity(repositoryClass: VegetalRepository::class)]
#[ApiResource(
        //security: "is_granted('ROLE_USER')",
        normalizationContext: ['groups'=>['read:vegetal:collection']],
        operations: [
            new GetCollection(),
            new GetCollection(
            name: 'count',
            controller: CountVegetalCollectionController::class,
            uriTemplate: '/count/vegetals', 
            openapiContext:[
                'summary' => 'Request for count all vegetals ',
                'parameters' => [
                    [
                        'name' => 'online',
                        'in' => 'query',
                        'description' => 'Filters Vegetals online',
                        'schema' => [
                            'type' => 'integer',
                            'maximum' => 1,
                            'minimum' => 0,
                        ],
                    ],
                ],
                'responses' => [
                    '200' => [
                        'description' => 'OK',
                        'content' => [ 
                            'application/json' => [ 
                                'schema' => [ 
                                    'type' => 'integer',
                                    'example' => 3
                                ]
                            ]
                        ]
                    ]
                ]
            ]

        ),
        new Get(normalizationContext: ['groups' => ['read:vegetal:collection', 'read:vegetal:item']]),
        new Put(denormalizationContext: ['groups' => ['put:vegetal:item']]),
        new Post(denormalizationContext: ['groups' => ['write:vegetal:item']]),
        //custom POST for validation publish
        new Post(
            denormalizationContext: ['groups' => ['write:vegetal:item']],
            name: 'publish',
            controller: VegetalPublishController::class,
            uriTemplate: '/publish/{id}/publish', 
            openapiContext:[
                'summary' => 'Request to publish a new vegetal',
                'requestBody' => [
                    'content' => [
                        'application/json' => [ 
                            'schema' => []
                        ]
                    ]
                ]
            ]
        ),
        new Put(
            denormalizationContext: ['groups' => ['write:vegetal:item']],
            name: 'image',
            uriTemplate: '/vegetals/{id}/image', 
            controller: VegetalImageController::class,
            inputFormats: ['multiparts' => ['multipart/form-data']],
            
        ),
        new Delete()
        
    ],
    paginationItemsPerPage:1,
    paginationMaximumItemsPerPage:10,
    paginationClientItemsPerPage:true
    ),
ApiFilter(SearchFilter::class, properties: ['id' => 'exact', 'name' => 'partial','scientificName'=> 'partial' ])
]
class Vegetal
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read:vegetal:collection'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[
        Groups(['read:vegetal:collection','write:vegetal:item' ]),
        Length(min:5) 
    ]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['read:vegetal:collection','write:vegetal:item'])]
    private ?string $scientificName = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['read:vegetal:item','put:vegetal:item','write:vegetal:item'])]
    private ?string $description = null;


    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="vegetal_image", fileNameProperty="image")
     */
    #[Groups(['read:vegetal:item','write:vegetal:item'])]
    private ?File $imageFile;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['read:vegetal:item','write:vegetal:item'])]
    private ?string $image = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['read:vegetal:item'])]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\ManyToOne(inversedBy: 'vegetals', cascade: ['persist'])]
    #[
        Groups(['read:vegetal:item','put:vegetal:item','write:vegetal:item']),
        Valid()
    ]
    private ?BotanicalFamily $botanicalFamily = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $slug = null;

    #[ORM\Column(options:["default"])]
    #[Groups(['read:vegetal:collection'])]
    private ?bool $online = false;

    public function __construct()
    {   
        $this->createdAt =  new \DateTime();
    }
    
    public function prePersist(){ 
        // TODO slug url doesnt work
        $title = $this->getName();
        $this->slug = $title; 
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getScientificName(): ?string
    {
        return $this->scientificName;
    }

    public function setScientificName(?string $scientificName): self
    {
        $this->scientificName = $scientificName;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getBotanicalFamily(): ?BotanicalFamily
    {
        return $this->botanicalFamily;
    }

    public function setBotanicalFamily(?BotanicalFamily $botanicalFamily): self
    {
        $this->botanicalFamily = $botanicalFamily;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function isOnline(): ?bool
    {
        return $this->online;
    }

    public function setOnline(bool $online): self
    {
        $this->online = $online;

        return $this;
    }

 /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }
}