<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Action\NotFoundAction;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use Doctrine\Common\Collections\Collection;
use App\Repository\BotanicalFamilyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;

#[ORM\Entity(repositoryClass: BotanicalFamilyRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(),
        new Get(
            controller: NotFoundAction::class,
            openapiContext: [
                'summary'=> 'hidden'
            ],
            read:false,
            output: false
        ),
        new Put(),
        new Delete(),
        new Post()
    ]
)]
class BotanicalFamily
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read:vegetal:item','put:vegetal:item']),
    
    ]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read:vegetal:item','put:vegetal:item', 'write:vegetal:item']),
    Length(min:5) 
    ]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'botanicalFamily', targetEntity: Vegetal::class)]
    private Collection $vegetals;

    public function __construct()
    {
        $this->vegetals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Vegetal>
     */
    public function getVegetals(): Collection
    {
        return $this->vegetals;
    }

    public function addVegetal(Vegetal $vegetal): self
    {
        if (!$this->vegetals->contains($vegetal)) {
            $this->vegetals->add($vegetal);
            $vegetal->setBotanicalFamily($this);
        }

        return $this;
    }

    public function removeVegetal(Vegetal $vegetal): self
    {
        if ($this->vegetals->removeElement($vegetal)) {
            // set the owning side to null (unless already changed)
            if ($vegetal->getBotanicalFamily() === $this) {
                $vegetal->setBotanicalFamily(null);
            }
        }

        return $this;
    }
}
