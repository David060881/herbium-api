<?php 

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Doctrine\Odm\Extension\PaginationExtension;

#[ApiResource(
    paginationEnabled:false,
    operations:[
        new Get(),
        new GetCollection()
    ]
)]
class Dependency
{
    #[ApiProperty(
        identifier:true
    )]
    private string $uuid;

    #[ApiProperty(
        description: 'Nom de la dépendance'
    )]
    private string $name;

    #[ApiProperty(
        description: 'Version de la dépendance',
        openapiContext: [
            'example' => "5.2.*"
        ]
    )]
    private string $version;

    public function __construct(
         string $uuid,
         string $name,
         string $version
    )
    { 
      $this->uuid = $uuid;
      $this->name = $name;
      $this->version = $version;
        
    }

    public function getUuid():string
    {
        return $this->uuid;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getVersion():string
    {
        return $this->version;
    }

}